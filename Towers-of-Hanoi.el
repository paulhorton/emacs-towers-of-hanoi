;;; Towers-of-Hanoi.el ---   --*-- lexical-binding: t --*--

;; Copyright (C) 2021, Paul Horton, All rights reserved.

;; Author: Paul Horton
;; Maintainer: Paul Horton
;; Created: 20210114
;; Updated: 20210114
;; Version: 0.0
;; Keywords: 

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;;  Just something I wrote for fun, inspired by a tangentially related example
;;  in "Higher Order Perl" by Mark Dominus.
;;
;;  This is the kind of program which is more fun to create than to use.
;;  It is also largely redundant to the classic hanoi.el by Damon Anton Permezel
;;  The display and implementation is largely independent though (I only noticed hanoi.el later)
;;  
;;  This program can perhaps serve as a simple example of an elisp program
;;  producing a pseudo-graphical display

;;; Change Log:

;;; Code:

(require 's)
(require 'let1)


;;────────────────────  Utils  ────────────────────
(defalias '≠ '/= "Standard math symbol for not equal.")
(defalias '≦ '<= "Standard math symbol for less than or equal")


(defun TOH/in-range? (num beg end)
  "Return true if NUM if number is in interval [BEG,END)"
  (and (numberp num)
       (<= beg num)
       (>  end num)))

(defun TOH/briefly-show (&rest msg-args)
  "briefly show message MSG"
  (apply #'message msg-args)
  (sit-for 0.8))



;;────────────────────  Variables & Their Handlers  ────────────────────

; ─────  disks  ─────
(defvar TOH/num-disks 4 "Number of disks")

(defun TOH/disk-big ()
  "Return (number of) biggest disk"
  (1- TOH/num-disks))

(defun TOH/disk-big-peg ()
  "Return peg that biggest disk is on"
  (aref TOH/disk→peg (TOH/disk-big)))
    

(defvar TOH/history-num-disks (list "5"))

(defun TOH/num-disks-read-from-minibuffer ()
  "Set number of disks to value read number from minibuffer, and return that number"
  (let* ((default (car TOH/history-num-disks))
         (num 
          (string-to-number
           (read-string
            (format "number of disks [2-99] (%s):  " default)
            nil 'TOH/history-num-disks default)))
         )
    (if (TOH/in-range? num 1 100)
        (setq TOH/num-disks num)
      (pop TOH/history-num-disks)
      (TOH/briefly-show "Please enter number in [2-99]")
      (TOH/num-disks-read-from-minibuffer)
      )))


; ─────  pegs  ─────
(defvar TOH/peg1  1  "Number of leftmost peg")
(defvar TOH/num-pegs  3 "Number of pegs")

(defun TOH/peg-other-than (&rest reserved-pegs)
  "Return a peg not equal to pegA or pegB"
  (car  (seq-difference  (number-sequence 1 TOH/num-pegs)  reserved-pegs)))


(defun TOH/peg-choose-from-minibuffer (peg-type)
  "Return peg number based on input by pressing number digit key"
  (let* ((default (if (string= peg-type "start")
                      1
                      (if (= (TOH/disk-big-peg) TOH/peg1)
                          TOH/num-pegs
                        TOH/peg1)))
         (touche 
          (read-key
           (format "select %s peg [1,%d]: (%d)" peg-type TOH/num-pegs default)))
         (num  (- touche 48)); "1"-->1, "2"-->2, ...
         )
    (if (member touche '(13 32)); return or space
        default
      (if (TOH/valid-peg? num)
          (if (and (string= peg-type "dest")
                   (= num (TOH/disk-big-peg)))
              (progn
                (TOH/briefly-show "Tower is already on peg %d" num)
                (TOH/peg-choose-from-minibuffer peg-type)
                )
            num)
        (TOH/briefly-show "Please press key in interval [1,%d]" TOH/num-pegs)
        (TOH/peg-choose-from-minibuffer peg-type)
        ))))


(defvar TOH/move-count 0 "Number of disk moves made so far")

(defvar TOH/display-erase-old? nil
  "If true, erase previous displays before drawing current towers")

(defun TOH/valid-peg? (peg)
  "Is PEG a valid peg?"
  (TOH/in-range? peg TOH/peg1 (1+ TOH/num-pegs)))


(defvar TOH/delay 1.5 "Number of seconds to wait after displaying each move")

(defun TOH/slower ()
  "Reduce TOH/delay to move faster"
  (interactive)
  (setq TOH/delay (* 1.5 TOH/delay))
  (TOH/briefly-show "delay per move is %g seconds" TOH/delay)
  )

(defun TOH/faster ()
  "Decrease TOH/delay to move faster"
  (interactive)
  (setq TOH/delay (* 0.67 TOH/delay))
  (TOH/briefly-show "delay per move is %g seconds" TOH/delay)
  )


(defvar TOH/disk→peg
  "To hold vector mapping disk number to peg number")

(defun TOH/peg→disks ()
  "Return vector mapping peg to its disks (computed from TOH/disk→peg)
Since pegs count from 1, the first element of the return vector is just a place holder"
  (let1  peg→disks  (make-vector (1+ TOH/num-pegs) nil)
    (cl-loop
     for disk from 0 below TOH/num-disks
     for peg = (aref TOH/disk→peg disk)
     do (push disk (aref peg→disks peg))
     )
    peg→disks))



;;────────────────────  Display  ────────────────────
(defun TOH/disk→string:bar-width (disk)
  "Width of bar representing peg and disk, or peg only if disk is NIL"
  (1+ (* 2 (if disk (+ 2 disk) 0))))

(defun TOH/disk→string (disk)
  "String representation of disk DISK on a peg
DISK is disk number, or NIL meaning no disk so just the peg"
  (s-center (+ 3 (TOH/disk→string:bar-width TOH/num-disks))
            (make-string (TOH/disk→string:bar-width disk) ?█)
            ))

(defun TOH/towers→string ()
  "String representation of all the towers"
  (cl-loop
   with peg→disks = (TOH/peg→disks)
   for disk-height from TOH/num-disks downto 0
   concat
   (concat
    (cl-loop
     for peg from TOH/peg1 to TOH/num-pegs
     for disk = (seq--elt-safe (aref peg→disks peg) disk-height)
     concat (TOH/disk→string disk)
     )
    "\n")
   ))

(defun TOH/display-towers ()
  "Display towers in Towers of Hanoi buffer (whether or not it is visible)"
  (with-current-buffer (TOH/buffer)
    (let1  buffer-read-only  nil
      (if TOH/display-erase-old?  (erase-buffer)  (goto-char (point-max)))
      (insert "\n" (TOH/towers→string))
      )))
  



;;────────────────────  Solver   ────────────────────
(defun TOH/num-moves-needed (num-disks num-pegs)
  "Return minimum number of moves needed to move NUM-DISKS disks using NUM-PEGS pegs"
  (cond
   ((= 3 num-pegs)
     (1- (expt 2 num-disks)))
   ((= 1 num-disks)
    1)
   (t
    (cl-loop
     for k from 1 below num-disks
     minimize (+  (* 2 (TOH/num-moves-needed k num-pegs))  (TOH/num-moves-needed (- num-disks k) (1- num-pegs)))
     ))))


(defun TOH/move-disk (disk-to-move peg)
  "Move DISK (number) to PEG (number)"
  (cl-assert (TOH/valid-peg? peg))
  (cl-loop
   for disk from 0 to disk-to-move
   do (cl-assert (≠ (aref TOH/disk→peg disk) peg) t); ensure disk-to-move, or smaller disk not on peg PEG
   )
  (aset TOH/disk→peg disk-to-move peg)
  (cl-incf TOH/move-count)
  (TOH/display-towers)
  (when (get-buffer-window (TOH/buffer))
    (forward-line (- TOH/num-disks))
    (recenter)
    (sit-for TOH/delay)
    (let1  ev  (read-event nil nil 0.002)
      (cl-case ev
        (?\C-r
         (message "Pausing.  Use exit-recursive-edit to resume")
         (recursive-edit)
         (switch-to-buffer (TOH/buffer)))
        (?+
         (TOH/faster))
        (?-
         (TOH/slower))
        (?p
         (read-key "Press a key to resume..."))
        (?r
         (throw 'reset nil))
        (??
         (TOH/show-help-message))
      ))))



(defun TOH/move-stack-3pegs (disk start-peg staging-peg dest-peg)
  "Move stack on start-peg to dest-peg, freely using staging-peg to do this.
Return true if successful."
  (cl-assert (>= disk 0))
  (cl-assert (>  TOH/num-disks disk))
  (if (= disk 0)
      (TOH/move-disk disk dest-peg)
    (TOH/move-stack-3pegs (1- disk) start-peg dest-peg staging-peg)
    (TOH/move-disk disk dest-peg)
      (TOH/move-stack-3pegs (1- disk) staging-peg start-peg dest-peg)
      )
    t)


(defun TOH/show-help-message ()
  (interactive)
  (let1  buffer-read-only  nil
    (save-excursion
      (goto-char (point-max))
      (insert TOH/help-message)
      (read-key "Press a key to resume...")
      (delete-region (point) (point-max))
      )))


;;────────────────────  Mode setup  ────────────────────
(defun TOH/buffer-name () "*Towers-of-Hanoi*")
  
(defun TOH/buffer () (get-buffer-create "*Towers-of-Hanoi*"))


(defvar TOH/keymap
  (let1  map  (make-sparse-keymap "Towers of Hanoi")
    (define-key map "m"  #'TOH/move-tower)
    (define-key map "r"	 #'TOH/reset)
    (define-key map "?"  #'TOH/show-help-message)
    (set-keymap-parent map special-mode-map)
    map))

(put 'TOH-mode 'mode-class 'special)

(define-derived-mode TOH/mode special-mode "Towers of Hanoi"
  "A mode for watching towers of Hanoi"
  (use-local-map TOH/keymap)
  (setq cursor-type nil)
  )


(defvar TOH/help-message
"
m    move tower
p    pause
r    (re)set up towers
+    faster
-    slower
?    help
")


;;────────────────────  User Commands  ────────────────────
(defun TOH/open ()
  "Set up and switch to Towers of Hanoi buffer"
  (interactive)
  (unless
      (prog1
          (buffer-live-p (TOH/buffer-name))
        (switch-to-buffer (TOH/buffer)))
    (let1  buffer-read-only  nil
      (erase-buffer)
      (insert "Welcome to Towers of Hanoi.\n"
              TOH/help-message)
    (TOH/mode)
    )))


(defun TOH/reset (num-disks start-peg)
  "Set-up tower with NUM-DISKS disks stacked on start-peg"
  (interactive
   (list
    (TOH/num-disks-read-from-minibuffer)
    (TOH/peg-choose-from-minibuffer "start")))
  (setq TOH/disk→peg (make-vector TOH/num-disks start-peg))
  (switch-to-buffer  (TOH/buffer))
  (buffer-disable-undo)
  (line-number-mode -1) (column-number-mode -1)
  (setq TOH/move-count 0)
  (let1  buffer-read-only  nil
      (erase-buffer)
      (TOH/display-towers)
    ))


(defun TOH/move-tower (dest-peg)
  "Move tower to dest-peg"
  (interactive
   (list (TOH/peg-choose-from-minibuffer "dest")))
  (unless
      (catch 'reset
        (cl-case TOH/num-pegs
          (3
           (TOH/move-stack-3pegs  (TOH/disk-big)
                                  (TOH/disk-big-peg)
                                  (TOH/peg-other-than (TOH/disk-big-peg) dest-peg)
                                  dest-peg))
          (t
           (error "TOH currently only implemented for 3 pegs."))
          ))
    (call-interactively #'TOH/reset)
    ))


;;; Towers-of-Hanoi.el ends here
